module ApplicationHelper

	# Returns the full title on a per-page basis
	def full_title(page_title = '') # definition of method with an optional argument
		base_title = "TortugaFeliz"
		if page_title.empty? # boolean test
			base_title # Implicit return
		else
			page_title + " | " + base_title # concatenate string"
		end
	end
end
